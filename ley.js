/**
 * Clase que contiene los metodos que dan funcionalidad a la interfaz
 *
 * @package     Administra Canales
 * @author      Cristobal Alvarez
 * @copyright   Administra Canales
 * @link
 * @since       13-11-2017
 * @version     1.0
 * @filesource pages/Login.js
*/
var i_Ley = (function($, window, document, undefined)
{
	'use strict';
	/**
	 * Constructor de la clase.
	 *
	 * @author  Cristobal Alavarez
	 * @since   24-01-30-01-2017
	 * @version 1.0
	 * @param   data: Arreglo de variables de configuracion de clase.
	 *
	 * @return void
	*/
	function Ley(data)
	{
    }

	Ley.prototype.init2 = function()
	{
        this.InicializarAce1();
		this.InicializarAce2();
		this.InicializarAce3();
		this.InicializarAce4();
		this.InicializarAce5();
		this.InicializarAce6();
	};

	/**
	 * Metodo que Carga la lista de programad.
	 *
	 * @author Cristobal Alvarez
	 * @since 30-01-2017
	 * @version 1.0
	 * @param data: Paquete de inyeccion de datos y metadatos.
	 *
	 * @return
	*/
    Ley.prototype.InicializarAce1 = function()
    {
        var editor = ace.edit("editorBloqueaDesbloquea");
        editor.setTheme("ace/theme/monokai");
        editor.session.setMode("ace/mode/javascript");
        editor.setOptions({
            maxLines: Infinity,
            showLineNumbers: false
        });
    }

    /**
	 * Metodo que Carga la lista de programad.
	 *
	 * @author Cristobal Alvarez
	 * @since 30-01-2017
	 * @version 1.0
	 * @param data: Paquete de inyeccion de datos y metadatos.
	 *
	 * @return
	*/
    Ley.prototype.InicializarAce2 = function()
    {
        var editor = ace.edit("editorDesbloquearBoton");
        editor.setTheme("ace/theme/monokai");
        editor.session.setMode("ace/mode/javascript");
        editor.setOptions({
            maxLines: Infinity,
            showLineNumbers: false,
            useWorker: false
        });
	}

	/**
	 * Metodo que Carga la lista de programad.
	 *
	 * @author Cristobal Alvarez
	 * @since 30-01-2017
	 * @version 1.0
	 * @param data: Paquete de inyeccion de datos y metadatos.
	 *
	 * @return
	*/
    Ley.prototype.InicializarAce3 = function()
    {
        var editor = ace.edit("editorAjaxSocketClass");
        editor.setTheme("ace/theme/monokai");
        editor.session.setMode("ace/mode/javascript");
        editor.setOptions({
            maxLines: Infinity,
            showLineNumbers: false,
            useWorker: false
        });
	}

	/**
	 * Metodo que Carga la lista de programad.
	 *
	 * @author Cristobal Alvarez
	 * @since 30-01-2017
	 * @version 1.0
	 * @param data: Paquete de inyeccion de datos y metadatos.
	 *
	 * @return
	*/
    Ley.prototype.InicializarAce4 = function()
    {
        var editor = ace.edit("editorClassBreadcrumbs");
        editor.setTheme("ace/theme/monokai");
        editor.session.setMode("ace/mode/html");
        editor.setOptions({
            maxLines: Infinity,
            showLineNumbers: false,
            useWorker: false
        });
	}

	/**
	 * Metodo que Carga la lista de programad.
	 *
	 * @author Cristobal Alvarez
	 * @since 30-01-2017
	 * @version 1.0
	 * @param data: Paquete de inyeccion de datos y metadatos.
	 *
	 * @return
	*/
    Ley.prototype.InicializarAce5 = function()
    {
        var editor = ace.edit("editorDataTable");
        editor.setTheme("ace/theme/monokai");
        editor.session.setMode("ace/mode/html");
        editor.setOptions({
            maxLines: Infinity,
            showLineNumbers: false,
            useWorker: false
        });
	}

	/**
	 * Metodo que Carga la lista de programad.
	 *
	 * @author Cristobal Alvarez
	 * @since 30-01-2017
	 * @version 1.0
	 * @param data: Paquete de inyeccion de datos y metadatos.
	 *
	 * @return
	*/
    Ley.prototype.InicializarAce6 = function()
    {
        var editor = ace.edit("editorDataTableJavascript");
        editor.setTheme("ace/theme/monokai");
        editor.session.setMode("ace/mode/javascript");
        editor.setOptions({
            maxLines: Infinity,
            showLineNumbers: false,
            useWorker: false
        });
	}
    return(Ley);
})(jQuery, window, document);