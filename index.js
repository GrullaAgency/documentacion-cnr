/**
 * Clase que contiene los metodos que dan funcionalidad a la interfaz
 *
 * @package     Administra Canales
 * @author      Cristobal Alvarez
 * @copyright   Administra Canales
 * @link
 * @since       13-11-2017
 * @version     1.0
 * @filesource pages/Login.js
*/
var i_Index = (function($, window, document, undefined)
{
	'use strict';
	/**
	 * Constructor de la clase.
	 *
	 * @author  Cristobal Alavarez
	 * @since   24-01-30-01-2017
	 * @version 1.0
	 * @param   data: Arreglo de variables de configuracion de clase.
	 *
	 * @return void
	*/
	function Index(data)
	{
    }

	Index.prototype.init2 = function()
	{
        this.InicializarAce1();
		this.InicializarAce2();
	};

	/**
	 * Metodo que Carga la lista de programad.
	 *
	 * @author Cristobal Alvarez
	 * @since 30-01-2017
	 * @version 1.0
	 * @param data: Paquete de inyeccion de datos y metadatos.
	 *
	 * @return
	*/
    Index.prototype.InicializarAce1 = function()
    {
        var editor = ace.edit("editorPlantillaJavascript");
        editor.setTheme("ace/theme/monokai");
        editor.session.setMode("ace/mode/javascript");
        editor.setOptions({
            maxLines: Infinity,
            showLineNumbers: false
        });
    }

    /**
	 * Metodo que Carga la lista de programad.
	 *
	 * @author Cristobal Alvarez
	 * @since 30-01-2017
	 * @version 1.0
	 * @param data: Paquete de inyeccion de datos y metadatos.
	 *
	 * @return
	*/
    Index.prototype.InicializarAce2 = function()
    {
        var editor = ace.edit("editorCodigoHtmlPlantillaJS");
        editor.setTheme("ace/theme/monokai");
        editor.session.setMode("ace/mode/html");
        editor.setOptions({
            maxLines: Infinity,
            showLineNumbers: false,
            useWorker: false
        });
	}

    return(Index);
})(jQuery, window, document);